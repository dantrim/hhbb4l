# hhbb4l

An area to start looking at the bb4l process in the context of the ATLAS HH->bbll analysis.

## Installation

### ATLAS Software (for processing xAODs/etc)

```verbatim
git clone ssh://git@gitlab.cern.ch:7999/dantrim/hhbb4l.git
cd hhbb4l/atlas_sw
lsetup "asetup AnalysisBase,21.2.192"
mkdir build
cd build
cmake ../source
make -j
source x86*/setup.sh
```
